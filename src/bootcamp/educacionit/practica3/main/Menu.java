package bootcamp.educacionit.practica3.main;

import bootcamp.educacionit.practica3.usuarios.Usuario;
import java.util.ArrayList;

public class Menu {
    public static void main(String[] arg) {

        ArrayList<Usuario> userList = new ArrayList<>();
        userList.add(new Usuario("Maria", "12345678"));
        userList.add(new Usuario("andres", "andres123"));
        userList.add(new Usuario("Carla", "Carla321"));


        MenuInterface.menuLogin(userList);


    }

}
