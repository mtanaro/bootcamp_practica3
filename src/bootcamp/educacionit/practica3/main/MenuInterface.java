package bootcamp.educacionit.practica3.main;

import bootcamp.educacionit.practica3.finders.UserFinder;
import bootcamp.educacionit.practica3.usuarios.Usuario;
import bootcamp.educacionit.practica3.validators.AmountValidator;
import bootcamp.educacionit.practica3.validators.LoginValidator;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

import static bootcamp.educacionit.practica3.messages.MessageOrchestrator.*;

public class MenuInterface {

    public static void showHeader(Usuario usuarioAutenticado) {
        System.out.println("Bienvendio a cajeros BA " + usuarioAutenticado.userName() + ". Elija una opcion");
    }
    public static void menuLogin(ArrayList<Usuario> userList){
        UserFinder finder=new UserFinder();
        System.out.println("HOLA");
        String passwordTyped ="";
        String userNameTyped="";
        int intentos = 0;
        Scanner leer = new Scanner(System.in);
        do {
            System.out.println("Ingrese su usuario: ");
            userNameTyped=leer.next();
            System.out.println("Ingrese su contrasenia: ");
            passwordTyped = leer.next();

            if (Boolean.TRUE.equals(LoginValidator.validateCredentials(userNameTyped,passwordTyped, userList))) {
                System.out.println(LOGIN_MESSAGE_OK);
                Usuario usuarioAutenticado= finder.findUser(userList,userNameTyped);
                MenuInterface.showHeader(usuarioAutenticado);
                MenuInterface.showOptions(leer,usuarioAutenticado);
                break;
            }
            else if (intentos == 2) {
                System.out.println(LOGIN_MESSAGE_ERROR_BLOCKED);
                break;
            } else {
                System.out.println(LOGIN_MESSAGE_ERROR);
                intentos++;
            }
        } while (intentos <= 2);

    }
    private static void showOptions(Scanner scan, Usuario usuarioAutenticado) {
        String menuNumber = "0";

        while (!menuNumber.equals("4")) {
            System.out.println(MENU_TAG);
            System.out.println(MENU_SELECTION_TAG);
            System.out.println(MENU_1);
            System.out.println(MENU_2);
            System.out.println(MENU_3);
            System.out.println(MENU_4);
            System.out.println(NUMBER_TAGS);
            menuNumber=scan.next();
            switch (menuNumber) {
                case "1" -> {
                    System.out.println("1 - "+ ESTADO_DE_CUENTA + usuarioAutenticado.userName() + ": ");
                    showFormattedBalance(usuarioAutenticado);
                    break;
                }
                case "2" -> {
                    System.out.println(ACTION_AMOUNT_REQUIRED.replace(VAR_TAG,DEPOSITO_TAG));
                    Double amount = scan.nextDouble();
                    usuarioAutenticado.getCajaDeAhorro().addToBalance(amount);
                    System.out.println(NUEVO_TAG+ ESTADO_DE_CUENTA + usuarioAutenticado.userName() + ": ");
                    showFormattedBalance(usuarioAutenticado);
                    break;
                }
                case "3"->{
                    System.out.println(ACTION_AMOUNT_REQUIRED.replace(VAR_TAG,RETIRO_TAG));
                    Double amount = scan.nextDouble();
                    if (Boolean.TRUE.equals(AmountValidator.validateAmount(amount,  usuarioAutenticado))) {
                        usuarioAutenticado.getCajaDeAhorro().substractBalance(amount);
                        System.out.println(NUEVO_TAG+ESTADO_DE_CUENTA + usuarioAutenticado.userName() + ": ");
                        showFormattedBalance(usuarioAutenticado);
                    }else
                        System.out.println(INCORRECT_AMOUNT);
                    break;
                }
                case "4" -> {
                    System.out.println(SALUDO_DESPEDIDA.replace(VAR_TAG, usuarioAutenticado.userName()));
                    break;
                }
                default -> System.out.println(OPCION_INCORRECTA);
            }
        }

    }

    private static void showFormattedBalance(Usuario usuarioAutenticado){
        DecimalFormat df=new DecimalFormat("0.00");
        System.out.println("$" + df.format(usuarioAutenticado.getCajaDeAhorro().getSaldo())+"\n");
    }
}
