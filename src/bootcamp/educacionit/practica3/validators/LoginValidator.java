package bootcamp.educacionit.practica3.validators;

import bootcamp.educacionit.practica3.finders.UserFinder;
import bootcamp.educacionit.practica3.usuarios.Usuario;

import java.util.ArrayList;

public class LoginValidator {

    private static Boolean validateUser(String userNameTyped, String userName) {
        return userName.equals(userNameTyped);
    }

    private static Boolean validatePassword(String passwordTyped, String password) {
        return password.equals(passwordTyped);
    }

    public static Boolean validateCredentials(String userNameTyped, String passwordTyped, ArrayList<Usuario> usuarios) {
        UserFinder finder=new UserFinder();
        Usuario usuario = finder.findUser(usuarios, userNameTyped);
        return validatePassword(passwordTyped, usuario.getPassword()) && validateUser(userNameTyped, usuario.userName());
    }


}
