package bootcamp.educacionit.practica3.validators;

import bootcamp.educacionit.practica3.usuarios.Usuario;

public class AmountValidator {
    public static Boolean validateAmount(Double amount, Usuario usuario){
        return amount<=usuario.getCajaDeAhorro().getSaldo();
    }
}
