package bootcamp.educacionit.practica3.finders;

import bootcamp.educacionit.practica3.usuarios.Usuario;

import java.util.ArrayList;

public class UserFinder {
    public Usuario findUser(ArrayList<Usuario> usuarios, String userNameTyped) {
        for (Usuario usr : usuarios)
            if (usr.userName().equals(userNameTyped))
                return usr;

        return new Usuario("false", "false");
    }
}
