package bootcamp.educacionit.practica3.usuarios;

import bootcamp.educacionit.practica3.cuentas.CajaAhorro;

import java.util.Random;

public class Usuario {
    private CajaAhorro cajaDeAhorro;
    private String password;
    private String userName;

    public Usuario(String userName, String password) {
        cajaDeAhorro = new CajaAhorro();
        this.password = password;
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }
    public String userName(){
        return userName;
    }

    public CajaAhorro getCajaDeAhorro() {
        return cajaDeAhorro;
    }

    public void setCajaDeAhorro(CajaAhorro cajaDeAhorro) {
        this.cajaDeAhorro = cajaDeAhorro;
    }


}
