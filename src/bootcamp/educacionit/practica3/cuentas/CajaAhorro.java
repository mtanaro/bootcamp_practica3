package bootcamp.educacionit.practica3.cuentas;

import java.util.concurrent.ThreadLocalRandom;

public class CajaAhorro {
    public Double saldo;

    public CajaAhorro() {
         saldo = ThreadLocalRandom.current().nextDouble(50,10000.00);
    }

    public Double getSaldo() {
        return saldo;
    }

    public void substractBalance(Double amount) {
        if (this.saldo > amount)
            this.saldo -= amount;
    }

    public void addToBalance(Double amount) {
        this.saldo += amount;
    }

}
