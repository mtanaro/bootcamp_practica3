package bootcamp.educacionit.practica3.messages;

public class MessageOrchestrator {
    public static final String LOGIN_MESSAGE_OK = "Bienvenido al sistema";
    public static final String LOGIN_MESSAGE_ERROR = "Usuario o password incorrecto";
    public static final String LOGIN_MESSAGE_ERROR_BLOCKED = "Ha realizado demasiados intentos, TARJETA bloqueado. Couniquese con el banco";
    public static final String OPCION_INCORRECTA = "Ha seleccionado una opcion incorrecta";
    public static final String INCORRECT_AMOUNT = "El valor ingresado supera el limite de cuenta";
    public static final String ESTADO_DE_CUENTA = "Estado de cuenta";

    /****MENU******/
    public static String MENU_1 = "1 - Estado de cuenta";
    public static String MENU_2 = "2 - Deposito";
    public static String MENU_3 = "3 - Retiro";
    public static String MENU_4 = "4 - Salir del sistema";

    public static String MENU_SELECTION_TAG="Seleccione una opcion\n";
    public static String MENU_TAG="###### MENU #######";
    public static String ACTION_AMOUNT_REQUIRED = "Ingrese el monto para %var%\n";
    public static String SALUDO_DESPEDIDA="Gracias %var% por utilizar nuestros servicios";
    public static String VAR_TAG="%var%";
    public static String RETIRO_TAG="retiro";
    public static String DEPOSITO_TAG="deposito";
    public static String NUMBER_TAGS="###################";
    public static String NUEVO_TAG="Nuevo ";
}
